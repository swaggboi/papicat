# papicat.pl

Perl script to interface with Akamai's Property API (aka PAPI)

```
Usage: papicat.pl <GET|PUT|HEAD|PATCH> [OPTIONS] [FILE(S)]

Print or commit delivery configuration JSON rule tree(s) via PAPI.
    -a, --account-switch-key=KEY    Specify account switch key
    -c, --contract-id=ID            Specify contract ID
    -D, --dry-run                   Dry run (make no real changes)
    -d, --delimiter=DELIM           Change delimiting character (default is TAB)
    -g, --group-id=ID               Specify group ID
    -H, --header                    Print header with output
    -h, --help                      Print this help message
    -P, --property-version=NUM      Specify property version
    -p, --property-id=ID            Specify property ID
    -V, --version                   Print version
    -v, --verbose                   Print more verbose output
```
